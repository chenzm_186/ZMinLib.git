# ZMinLib

[![CI Status](https://img.shields.io/travis/rattanchen/ZMinLib.svg?style=flat)](https://travis-ci.org/rattanchen/ZMinLib)
[![Version](https://img.shields.io/cocoapods/v/ZMinLib.svg?style=flat)](https://cocoapods.org/pods/ZMinLib)
[![License](https://img.shields.io/cocoapods/l/ZMinLib.svg?style=flat)](https://cocoapods.org/pods/ZMinLib)
[![Platform](https://img.shields.io/cocoapods/p/ZMinLib.svg?style=flat)](https://cocoapods.org/pods/ZMinLib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ZMinLib is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ZMinLib'
```

## Author

rattanchen, 1005332621@qq.com

## License

ZMinLib is available under the MIT license. See the LICENSE file for more info.

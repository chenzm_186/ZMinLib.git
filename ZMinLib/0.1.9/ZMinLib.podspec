Pod::Spec.new do |s|
  s.name             = 'ZMinLib'
  s.version          = '0.1.9'
  s.summary          = '简述说明ZMinLib：例子'
  s.description      = <<-DESC
TODO: 详细描述.详细描述还不能比简述的内容短，不然还会有警告，真实有心了，哈哈哈～
                       DESC
  s.homepage         = 'https://mp.csdn.net/console/home'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'rattanchen' => '1005332621@qq.com' }
  s.source           = { :git => 'https://gitee.com/chenzm_186/ZMinLib.git', :tag => s.version.to_s }
  s.ios.deployment_target = '9.0'
  
  # ===================  所有子项目  =======================
  s.subspec 'WCDB' do |ss|
    # 1.0.6
    ss.vendored_frameworks = 'Products/WCDB/*.framework'
    ss.libraries = "z", "c++"
    ss.frameworks = "CoreFoundation", "Security", "Foundation"
  end

  s.subspec 'ZipArchive' do |ss|
    # 1.4.0
    ss.source_files = 'Products/ZipArchive/*.{h,hpp}'
    ss.public_header_files = 'Products/ZipArchive/*.{h,hpp}'
    ss.vendored_library = 'Products/ZipArchive/*.a'
  end
  # ===================  所有子项目  =======================


  # git仓库地址
  s.source           = { :git => 'https://gitee.com/chenzm_186/ZMinLib.git', :tag => s.version.to_s }
  # 设置默认下载实例,没有设置则下载所有
  # All
  s.default_subspec='All'
  s.subspec 'All' do |ss|
    ss.dependency 'ZMinLib/WCDB'
    ss.dependency 'ZMinLib/ZipArchive'
  end

end
